import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotifikasisiswaPageRoutingModule } from './notifikasisiswa-routing.module';

import { NotifikasisiswaPage } from './notifikasisiswa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotifikasisiswaPageRoutingModule
  ],
  declarations: [NotifikasisiswaPage]
})
export class NotifikasisiswaPageModule {}
