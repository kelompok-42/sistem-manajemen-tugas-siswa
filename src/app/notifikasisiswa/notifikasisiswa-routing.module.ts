import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotifikasisiswaPage } from './notifikasisiswa.page';

const routes: Routes = [
  {
    path: '',
    component: NotifikasisiswaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotifikasisiswaPageRoutingModule {}
