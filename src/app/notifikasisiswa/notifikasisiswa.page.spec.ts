import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NotifikasisiswaPage } from './notifikasisiswa.page';

describe('NotifikasisiswaPage', () => {
  let component: NotifikasisiswaPage;
  let fixture: ComponentFixture<NotifikasisiswaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotifikasisiswaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NotifikasisiswaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
