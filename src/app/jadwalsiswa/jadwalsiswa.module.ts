import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JadwalsiswaPageRoutingModule } from './jadwalsiswa-routing.module';

import { JadwalsiswaPage } from './jadwalsiswa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JadwalsiswaPageRoutingModule
  ],
  declarations: [JadwalsiswaPage]
})
export class JadwalsiswaPageModule {}
