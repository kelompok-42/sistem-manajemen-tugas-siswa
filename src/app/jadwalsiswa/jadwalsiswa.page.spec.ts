import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JadwalsiswaPage } from './jadwalsiswa.page';

describe('JadwalsiswaPage', () => {
  let component: JadwalsiswaPage;
  let fixture: ComponentFixture<JadwalsiswaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JadwalsiswaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JadwalsiswaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
