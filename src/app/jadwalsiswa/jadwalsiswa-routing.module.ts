import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JadwalsiswaPage } from './jadwalsiswa.page';

const routes: Routes = [
  {
    path: '',
    component: JadwalsiswaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JadwalsiswaPageRoutingModule {}
