import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormnilaiPageRoutingModule } from './formnilai-routing.module';

import { FormnilaiPage } from './formnilai.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormnilaiPageRoutingModule
  ],
  declarations: [FormnilaiPage]
})
export class FormnilaiPageModule {}
