import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormnilaiPage } from './formnilai.page';

const routes: Routes = [
  {
    path: '',
    component: FormnilaiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormnilaiPageRoutingModule {}
