import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AngularFirestore} from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController, LoadingController, NavController } from '@ionic/angular';

import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Router} from '@angular/router';
import { Penilaian } from '../models/penilaian';

@Component({
  selector: 'app-formnilai',
  templateUrl: './formnilai.page.html',
  styleUrls: ['./formnilai.page.scss'],
})
export class FormnilaiPage implements OnInit {

  lapornilai ={} as Penilaian;
  constructor(
    public modalController: ModalController,
    public db: AngularFirestore,
    public auth: AngularFireAuth,
    public toastr: ToastController,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    private actRoute: ActivatedRoute,
    public alert:AlertController,
    public router: Router
  ) { }

  ngOnInit() {
  }

  async createLapor(lapornilai: Penilaian) {
    // console.log(kelas);

    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.db.collection("task").add(lapornilai);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      loader.dismiss();

      // redirect to kelas page
      this.navCtrl.navigateRoot("laporannilai");
    }
  }

  formValidation() {
    if (!this.lapornilai.pengirim) {
      // show toast message
      this.showToast("Enter Pengirim");
      return false;
    }

    
    if (!this.lapornilai.penerima) {
      // show toast message
      this.showToast("Enter Penerima");
      return false;
    }

    if (!this.lapornilai.subjek) {
      // show toast message
      this.showToast("Enter Subjek");
      return false;
    }


    if (!this.lapornilai.nama) {
      // show toast message
      this.showToast("Enter Nama");
      return false;
    }

    if (!this.lapornilai.kelas) {
      // show toast message
      this.showToast("Enter Kelas");
      return false;
    }

    if (!this.lapornilai.semester) {
      // show toast message
      this.showToast("Enter Semester");
      return false;
    }

    if (!this.lapornilai.nilai) {
      // show toast message
      this.showToast("Enter Nilai");
      return false;
    }

    if (!this.lapornilai.status) {
      // show toast message
      this.showToast("Enter Status");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastr
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }

}
