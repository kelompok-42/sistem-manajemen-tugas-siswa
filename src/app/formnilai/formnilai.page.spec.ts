import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FormnilaiPage } from './formnilai.page';

describe('FormnilaiPage', () => {
  let component: FormnilaiPage;
  let fixture: ComponentFixture<FormnilaiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormnilaiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FormnilaiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
