import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegistersiswaPage } from './registersiswa.page';

describe('RegistersiswaPage', () => {
  let component: RegistersiswaPage;
  let fixture: ComponentFixture<RegistersiswaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistersiswaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegistersiswaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
