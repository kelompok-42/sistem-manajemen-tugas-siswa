import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistersiswaPage } from './registersiswa.page';

const routes: Routes = [
  {
    path: '',
    component: RegistersiswaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistersiswaPageRoutingModule {}
