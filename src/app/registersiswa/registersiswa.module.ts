import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistersiswaPageRoutingModule } from './registersiswa-routing.module';

import { RegistersiswaPage } from './registersiswa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistersiswaPageRoutingModule
  ],
  declarations: [RegistersiswaPage]
})
export class RegistersiswaPageModule {}
