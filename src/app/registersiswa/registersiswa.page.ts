import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registersiswa',
  templateUrl: './registersiswa.page.html',
  styleUrls: ['./registersiswa.page.scss'],
})
export class RegistersiswaPage implements OnInit {
  name: string;
  nipnisn: number;
  email: string;
  phone: number;
  password: string;

  constructor(
    public toastr: ToastController,
    public loadingCtrl: LoadingController,
    public auth: AngularFireAuth,
    public db: AngularFirestore,
    public alert:AlertController,
    public router: Router
  ) { }

  ngOnInit() {
  }

  user: any = {};
  async register()
  {
    if(this.name && this.nipnisn && this.email && this.phone && this.password)
    {
      const loading = await this.loadingCtrl.create({
        message: 'proses...',
        spinner: 'crescent',
        showBackdrop: true
      });

      loading.present();

    this.auth
    .auth.createUserWithEmailAndPassword(this.email, this.password).then(data=>{
      data.user.sendEmailVerification();
      this.db.collection('user').doc(data.user.uid).set({
        'userId': data.user.uid,
        'userName': this.name,
        'userNipNisn': this.nipnisn,
        'userEmail': this.email,
        'userPhone': this.phone,
        'createdAt': Date.now()
      })
      .then(()=> {
        loading.dismiss();
        this.toast('Register Berhasil');
        this.router.navigate(['/loginsiswa']);
      })
      .catch(err => {
        loading.dismiss();
        this.pesanKesalahan();
      })
    })
  }
}

  async toast(message)
  {
    const toast = await this.toastr.create({
      message: message,
      position: 'top',
      buttons: ["OK"]
    });

    toast.present();
  }

  async pesanKesalahan() {
    const toast = await this.toastr.create({
      message: 'Tidak dapat melakukan registrasi. Coba lagi!',
    });
    toast.present();
  }

}
