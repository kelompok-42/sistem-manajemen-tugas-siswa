import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  constructor(
    public alertController: AlertController,
    public router: Router
    ) { }

  ngOnInit() {
  }

  async presentAlertMultipleButtons() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      message: 'Anda yakin ingin keluar?',
      buttons: [
        {  
          text: 'Batal',  
          role: 'Batal',  
          handler: () => {  
            console.log('Cancel');  
          }  
        },  
        {  
          text: 'Ya',  
          handler: () => {  
            this.router.navigate(['/welcome']);
            console.log('Ya');  
          }  
        }  
      ]
    });

    await alert.present();
  }

}
