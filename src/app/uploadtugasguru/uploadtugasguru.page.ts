import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AngularFirestore} from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController, LoadingController, NavController } from '@ionic/angular';

import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Router} from '@angular/router';
import { Tampiltugas } from '../models/tampiltugas';

@Component({
  selector: 'app-uploadtugasguru',
  templateUrl: './uploadtugasguru.page.html',
  styleUrls: ['./uploadtugasguru.page.scss'],
})
export class UploadtugasguruPage implements OnInit {

  tampiltugas ={} as Tampiltugas;
  constructor(
    public modalController: ModalController,
    public db: AngularFirestore,
    public auth: AngularFireAuth,
    public toastr: ToastController,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    private actRoute: ActivatedRoute,
    public alert:AlertController,
    public router: Router
  ) { }

  ngOnInit() {

  }

  async createUnggah(tampiltugas: Tampiltugas) {
    // console.log(kelas);

    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.db.collection("task").add(tampiltugas);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      loader.dismiss();
      this.toast('Upload Berhasil');

      // redirect to kelas page
      this.navCtrl.navigateRoot("uploadtugasguru");
    }
  }

  formValidation() {
    if (!this.tampiltugas.tampilTugas) {
      // show toast message
      this.showToast("Enter Nama Siswa");
      return false;
    }

    return true;
  }

  async toast(message)
  {
    const toastr = await this.toastr.create({
      message: message,
      position: 'top',
      buttons: ["OK"]
    });

    toastr.present();
  }

  showToast(message: string) {
    this.toastr
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }

}
