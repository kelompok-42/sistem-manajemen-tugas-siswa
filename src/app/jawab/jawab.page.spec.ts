import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JawabPage } from './jawab.page';

describe('JawabPage', () => {
  let component: JawabPage;
  let fixture: ComponentFixture<JawabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JawabPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JawabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
