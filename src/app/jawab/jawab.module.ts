import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JawabPageRoutingModule } from './jawab-routing.module';

import { JawabPage } from './jawab.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JawabPageRoutingModule
  ],
  declarations: [JawabPage]
})
export class JawabPageModule {}
