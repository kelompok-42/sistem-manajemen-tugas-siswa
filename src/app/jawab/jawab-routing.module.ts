import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JawabPage } from './jawab.page';

const routes: Routes = [
  {
    path: '',
    component: JawabPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JawabPageRoutingModule {}
