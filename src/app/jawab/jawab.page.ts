import { Component, OnInit } from '@angular/core';
import { AngularFirestore} from '@angular/fire/firestore';
import { ToastController, LoadingController, NavController } from '@ionic/angular';
import { Tampiljawab } from '../models/tampiljawab';

@Component({
  selector: 'app-jawab',
  templateUrl: './jawab.page.html',
  styleUrls: ['./jawab.page.scss'],
})
export class JawabPage implements OnInit {

  tampiljawab ={} as Tampiljawab;
  constructor(
    public db: AngularFirestore,
    public toastr: ToastController,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
  }

  async createUnggah(tampiljawab: Tampiljawab) {
    // console.log(kelas);

    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.db.collection("task").add(tampiljawab);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      loader.dismiss();
      this.toast('Upload Berhasil');

      // redirect to kelas page
      this.navCtrl.navigateRoot("jawab");
    }
  }

  formValidation() {
    if (!this.tampiljawab.njawab) {
      // show toast message
      this.showToast("Jawaban Terkirim");
      return false;
    }

    return true;
  }

  async toast(message)
  {
    const toastr = await this.toastr.create({
      message: message,
      position: 'top',
      buttons: ["OK"]
    });

    toastr.present();
  }

  showToast(message: string) {
    this.toastr
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }

}
