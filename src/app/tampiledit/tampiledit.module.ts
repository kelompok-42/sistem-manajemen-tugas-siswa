import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TampileditPageRoutingModule } from './tampiledit-routing.module';

import { TampileditPage } from './tampiledit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TampileditPageRoutingModule
  ],
  declarations: [TampileditPage]
})
export class TampileditPageModule {}
