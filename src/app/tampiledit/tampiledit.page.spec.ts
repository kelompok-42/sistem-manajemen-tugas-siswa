import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TampileditPage } from './tampiledit.page';

describe('TampileditPage', () => {
  let component: TampileditPage;
  let fixture: ComponentFixture<TampileditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TampileditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TampileditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
