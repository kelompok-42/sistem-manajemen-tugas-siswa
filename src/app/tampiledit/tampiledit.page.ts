import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
import { Tampiltugas } from '../models/tampiltugas';

@Component({
  selector: 'app-tampiledit',
  templateUrl: './tampiledit.page.html',
  styleUrls: ['./tampiledit.page.scss'],
})
export class TampileditPage implements OnInit {

  tampiltugas = {} as Tampiltugas;
  id: any;
  constructor(
    private actRoute: ActivatedRoute,
    private db: AngularFirestore,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private navCtrl: NavController
  ) { 
    this.id = this.actRoute.snapshot.paramMap.get("id");
  }

  ngOnInit() {
    this.getKelasById(this.id);
  }

  async getKelasById(id: string) {
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    this.db.doc("task/" + id).valueChanges().subscribe(data => {
        this.tampiltugas.tampilTugas = data["tampiltugas"];

        loader.dismiss();
      });
  }

  async createUpdate(tampiltugas: Tampiltugas) {
    if (this.formValidation()) {
      
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.db.doc("task/" + this.id).update(tampiltugas);
      } catch (e) {
        this.showToast(e);
      }
      await loader.dismiss();

      this.navCtrl.navigateRoot("uploadtugasguru");
    }
  }

  formValidation() {
    if (!this.tampiltugas.tampilTugas) {
      // show toast message
      this.showToast("Enter Tugas");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }

}
