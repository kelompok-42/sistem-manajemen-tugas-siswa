import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TampileditPage } from './tampiledit.page';

const routes: Routes = [
  {
    path: '',
    component: TampileditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TampileditPageRoutingModule {}
