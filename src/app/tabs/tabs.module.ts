import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
    {
      path: 'dashboard',
      children: [
        {
          path: '',
          loadChildren: '../dashboard/dashboard.module#DashboardPageModule'
        }
      ]
    },
    {
      path: 'profil',
      children: [
        {
          path: '',
          loadChildren: '../profil/profil.module#ProfilPageModule'
        }
      ]
    },
    {
      path: 'formnilai',
      children: [
        {
          path: '',
          loadChildren: '../formnilai/formnilai.module#FormnilaiPageModule'
        }
      ]
    },
  ]
  },
  {
    path: '',
    redirectTo: 'tabs/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage],
  exports: [RouterModule]
})
export class TabsPageModule {}
