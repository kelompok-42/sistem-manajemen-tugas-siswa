import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HalamantugasPage } from './halamantugas.page';

const routes: Routes = [
  {
    path: '',
    component: HalamantugasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HalamantugasPageRoutingModule {}
