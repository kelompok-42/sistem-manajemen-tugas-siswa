import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HalamantugasPageRoutingModule } from './halamantugas-routing.module';

import { HalamantugasPage } from './halamantugas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HalamantugasPageRoutingModule
  ],
  declarations: [HalamantugasPage]
})
export class HalamantugasPageModule {}
