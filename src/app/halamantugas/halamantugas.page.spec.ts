import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HalamantugasPage } from './halamantugas.page';

describe('HalamantugasPage', () => {
  let component: HalamantugasPage;
  let fixture: ComponentFixture<HalamantugasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HalamantugasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HalamantugasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
