import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController, Platform, NavController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-jawaban',
  templateUrl: './jawaban.page.html',
  styleUrls: ['./jawaban.page.scss'],
})
export class JawabanPage implements OnInit {

  tampiljawab: any;
  subscription: any;
  constructor(
    private toastCtrl: ToastController,
    private db: AngularFirestore,
    private loadingCtrl: LoadingController,
    private platform: Platform,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
  }

    ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator["app"].exitApp();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  async getTampiljawab() {
    // console.log("get kelass");

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    try {
      this.db.collection("task").snapshotChanges().subscribe(data => {
          this.tampiljawab = data.map(e => {
            return {
              id: e.payload.doc.id,
              njawab: e.payload.doc.data()["njawab"]
            };
          });

          // dismiss loader
          loader.dismiss();
        });
    } catch (e) {
      this.showToast(e);
    }
  }

  ionViewWillEnter() {
    this.getTampiljawab();
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }

}
