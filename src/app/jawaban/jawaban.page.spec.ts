import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JawabanPage } from './jawaban.page';

describe('JawabanPage', () => {
  let component: JawabanPage;
  let fixture: ComponentFixture<JawabanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JawabanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JawabanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
