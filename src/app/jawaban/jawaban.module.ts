import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JawabanPageRoutingModule } from './jawaban-routing.module';

import { JawabanPage } from './jawaban.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JawabanPageRoutingModule
  ],
  declarations: [JawabanPage]
})
export class JawabanPageModule {}
