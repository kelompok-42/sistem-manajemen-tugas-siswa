export interface Penilaian {
    pengirim: string;
    penerima:string;
    subjek: string;
    nama: string;
    kelas: string;
    semester: string;
    nilai: number;
    status: string;
    pesan: string;
    createdAt: number;
}
