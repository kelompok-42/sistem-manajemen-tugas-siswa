export interface User {
    userId: string;
    userName: string;
    userNipNisn: number;
    userEmail: string;
    userPhone: number;
    createdAt: number;
}
