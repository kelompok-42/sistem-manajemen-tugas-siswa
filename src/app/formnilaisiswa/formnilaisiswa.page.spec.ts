import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FormnilaisiswaPage } from './formnilaisiswa.page';

describe('FormnilaisiswaPage', () => {
  let component: FormnilaisiswaPage;
  let fixture: ComponentFixture<FormnilaisiswaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormnilaisiswaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FormnilaisiswaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
