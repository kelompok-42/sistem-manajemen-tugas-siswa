import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormnilaisiswaPage } from './formnilaisiswa.page';

const routes: Routes = [
  {
    path: '',
    component: FormnilaisiswaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormnilaisiswaPageRoutingModule {}
