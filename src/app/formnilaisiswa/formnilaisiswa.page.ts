import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController, Platform } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-formnilaisiswa',
  templateUrl: './formnilaisiswa.page.html',
  styleUrls: ['./formnilaisiswa.page.scss'],
})
export class FormnilaisiswaPage implements OnInit {

  lapornilai: any;
  subscription: any;
  constructor(
    private toastr: ToastController,
    private db: AngularFirestore,
    private loadingCtrl: LoadingController,
    private platform: Platform
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator["app"].exitApp();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  async getPenilaian() {
    // console.log("get kelass");

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    try {
      this.db
        .collection("task")

        .snapshotChanges()
        .subscribe(data => {
          this.lapornilai = data.map(e => {
            return {
              id: e.payload.doc.id,
              pengirim: e.payload.doc.data()["pengirim"],
              penerima: e.payload.doc.data()["penerima"],
              subjek: e.payload.doc.data()["subjek"],
              nama: e.payload.doc.data()["nama"],
              kelas: e.payload.doc.data()["kelas"],
              semester: e.payload.doc.data()["semester"],
              nilai: e.payload.doc.data()["nilai"],
              status: e.payload.doc.data()["status"],
              pesan: e.payload.doc.data()["pesan"]

            };
          });

          // dismiss loader
          loader.dismiss();
        });
    } catch (e) {
      this.showToast(e);
    }
  }

  ionViewWillEnter() {
    this.getPenilaian();
  }

  showToast(message: string) {
    this.toastr
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }

}
