import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormnilaisiswaPageRoutingModule } from './formnilaisiswa-routing.module';

import { FormnilaisiswaPage } from './formnilaisiswa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormnilaisiswaPageRoutingModule
  ],
  declarations: [FormnilaisiswaPage]
})
export class FormnilaisiswaPageModule {}
