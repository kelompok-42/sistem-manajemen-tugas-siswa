import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'dashboardsiswa',
    loadChildren: () => import('./dashboardsiswa/dashboardsiswa.module').then( m => m.DashboardsiswaPageModule)
  },
  { 
    path: 'jadwalguru', 
    loadChildren: () => import('./jadwalguru/jadwalguru.module').then( m => m.JadwalguruPageModule)
  },
  {
    path: 'jadwalsiswa',
    loadChildren: () => import('./jadwalsiswa/jadwalsiswa.module').then( m => m.JadwalsiswaPageModule)
  },
  {
    path: 'kelas',
    loadChildren: () => import('./kelas/kelas.module').then( m => m.KelasPageModule)
  },
  {
    path: 'kelassiswa',
    loadChildren: () => import('./kelassiswa/kelassiswa.module').then( m => m.KelassiswaPageModule)
  },
  {
    path: 'laporannilai',
    loadChildren: () => import('./laporannilai/laporannilai.module').then( m => m.LaporannilaiPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'loginsiswa',
    loadChildren: () => import('./loginsiswa/loginsiswa.module').then( m => m.LoginsiswaPageModule)
  },
  {
    path: 'logout',
    loadChildren: () => import('./logout/logout.module').then( m => m.LogoutPageModule)
  },
  {
    path: 'materi',
    loadChildren: () => import('./materi/materi.module').then( m => m.MateriPageModule)
  },
  {
    path: 'menuguru',
    loadChildren: () => import('./menuguru/menuguru.module').then( m => m.MenuguruPageModule)
  },
  {
    path: 'menusiswa',
    loadChildren: () => import('./menusiswa/menusiswa.module').then( m => m.MenusiswaPageModule)
  },
  {
    path: 'notifikasiguru',
    loadChildren: () => import('./notifikasiguru/notifikasiguru.module').then( m => m.NotifikasiguruPageModule)
  },
  {
    path: 'pilihanganda',
    loadChildren: () => import('./pilihanganda/pilihanganda.module').then( m => m.PilihangandaPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  //{ path: 'tabs', loadChildren: () => import('./tabs/tabs.module').then( m => m.TabsPageModule) },
  {
    path: 'tambahkodemapel',
    loadChildren: () => import('./tambahkodemapel/tambahkodemapel.module').then( m => m.TambahkodemapelPageModule)
  },
  {
    path: 'tugasguru',
    loadChildren: () => import('./tugasguru/tugasguru.module').then( m => m.TugasguruPageModule)
  },
  {
    path: 'tugassiswa',
    loadChildren: () => import('./tugassiswa/tugassiswa.module').then( m => m.TugassiswaPageModule)
  },
  {
    path: 'uploadtugasguru',
    loadChildren: () => import('./uploadtugasguru/uploadtugasguru.module').then( m => m.UploadtugasguruPageModule)
  },
  {
    path: 'welcome',
    loadChildren: () => import('./welcome/welcome.module').then( m => m.WelcomePageModule)
  },
  {
    path: 'pilihangandasiswa',
    loadChildren: () => import('./pilihangandasiswa/pilihangandasiswa.module').then( m => m.PilihangandasiswaPageModule)
  },
  {
    path: 'registersiswa',
    loadChildren: () => import('./registersiswa/registersiswa.module').then( m => m.RegistersiswaPageModule)
  },
  {
    path: 'forgot',
    loadChildren: () => import('./forgot/forgot.module').then( m => m.ForgotPageModule)
  },
  {
    path: 'forgotsiswa',
    loadChildren: () => import('./forgotsiswa/forgotsiswa.module').then( m => m.ForgotsiswaPageModule)
  },
  {
    path: 'essay',
    loadChildren: () => import('./essay/essay.module').then( m => m.EssayPageModule)
  },
  {
    path: 'tampiltugas',
    loadChildren: () => import('./tampiltugas/tampiltugas.module').then( m => m.TampiltugasPageModule)
  },
  {
    path: 'halamantugas',
    loadChildren: () => import('./halamantugas/halamantugas.module').then( m => m.HalamantugasPageModule)
  },
  {
    path: 'tab1',
    loadChildren: () => import('./tab1/tab1.module').then( m => m.Tab1PageModule)
  },
  {
    path: 'profil',
    loadChildren: () => import('./profil/profil.module').then( m => m.ProfilPageModule)
  },
  {
    path: 'profiledit',
    loadChildren: () => import('./profiledit/profiledit.module').then( m => m.ProfileditPageModule)
  },
  {
    path: 'tampiledit',
    loadChildren: () => import('./tampiledit/tampiledit.module').then( m => m.TampileditPageModule)
  },
  {
    path: 'jawab',
    loadChildren: () => import('./jawab/jawab.module').then( m => m.JawabPageModule)
  },
  {
    path: 'formnilai',
    loadChildren: () => import('./formnilai/formnilai.module').then( m => m.FormnilaiPageModule)
  },
  {
    path: 'notifikasisiswa',
    loadChildren: () => import('./notifikasisiswa/notifikasisiswa.module').then( m => m.NotifikasisiswaPageModule)
  },
  {
    path: 'formnilaisiswa',
    loadChildren: () => import('./formnilaisiswa/formnilaisiswa.module').then( m => m.FormnilaisiswaPageModule)
  },
  {
    path: 'jawaban',
    loadChildren: () => import('./jawaban/jawaban.module').then( m => m.JawabanPageModule)
  },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules,scrollPositionRestoration: 'enabled' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
