import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController, Platform, NavController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';

import { ModalController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';

import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Router} from '@angular/router';
import { Penilaian } from '../models/penilaian';

@Component({
  selector: 'app-laporannilai',
  templateUrl: './laporannilai.page.html',
  styleUrls: ['./laporannilai.page.scss'],
})
export class LaporannilaiPage implements OnInit {

  kirim ={} as Penilaian;
  lapornilai: any;
  subscription: any;
  constructor(
    private toastr: ToastController,
    private db: AngularFirestore,
    private loadingCtrl: LoadingController,
    private platform: Platform,
    public modalController: ModalController,
    public auth: AngularFireAuth,
    public navCtrl: NavController,
    private actRoute: ActivatedRoute,
    public alert:AlertController,
    public router: Router
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator["app"].exitApp();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  async getPenilaian() {
    // console.log("get kelass");

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    try {
      this.db
        .collection("task")

        .snapshotChanges()
        .subscribe(data => {
          this.lapornilai = data.map(e => {
            return {
              id: e.payload.doc.id,
              pengirim: e.payload.doc.data()["pengirim"],
              penerima: e.payload.doc.data()["penerima"],
              subjek: e.payload.doc.data()["subjek"],
              nama: e.payload.doc.data()["nama"],
              kelas: e.payload.doc.data()["kelas"],
              semester: e.payload.doc.data()["semester"],
              nilai: e.payload.doc.data()["nilai"],
              status: e.payload.doc.data()["status"],

            };
          });

          // dismiss loader
          loader.dismiss();
        });
    } catch (e) {
      this.showToast(e);
    }
  }

  ionViewWillEnter() {
    this.getPenilaian();
  }



  async createKirim(kirim: Penilaian) {
    // console.log(kelas);

    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.db.collection("task").add(kirim);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      loader.dismiss();

      // redirect to kelas page
      this.navCtrl.navigateRoot("formnilaisiswa");
    }
  }

  formValidation() {
    if (!this.lapornilai.pesan) {
      // show toast message
      this.showToast("Nilai Terkirim...");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastr
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }

}
