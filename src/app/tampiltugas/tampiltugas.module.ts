import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TampiltugasPageRoutingModule } from './tampiltugas-routing.module';

import { TampiltugasPage } from './tampiltugas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TampiltugasPageRoutingModule
  ],
  declarations: [TampiltugasPage]
})
export class TampiltugasPageModule {}
