import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TampiltugasPage } from './tampiltugas.page';

const routes: Routes = [
  {
    path: '',
    component: TampiltugasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TampiltugasPageRoutingModule {}
