import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController, Platform, NavController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { Tampiltugas } from '../models/tampiltugas';

@Component({
  selector: 'app-tampiltugas',
  templateUrl: './tampiltugas.page.html',
  styleUrls: ['./tampiltugas.page.scss'],
})
export class TampiltugasPage implements OnInit {

  tampiltugas: any;
  subscription: any;
  constructor(
    private toastCtrl: ToastController,
    private db: AngularFirestore,
    private loadingCtrl: LoadingController,
    private platform: Platform,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator["app"].exitApp();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  async getTampiltugas() {
    // console.log("get kelass");

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    try {
      this.db.collection("task").snapshotChanges().subscribe(data => {
          this.tampiltugas = data.map(e => {
            return {
              id: e.payload.doc.id,
              tampiltugas: e.payload.doc.data()["tampilTugas"]
            };
          });

          // dismiss loader
          loader.dismiss();
        });
    } catch (e) {
      this.showToast(e);
    }
  }

  ionViewWillEnter() {
    this.getTampiltugas();
  }

  async createKirim() {
    // console.log(kelas);

    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.db.collection("task");
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      loader.dismiss();

      // redirect to kelas page
      this.navCtrl.navigateRoot("materi");
    }
  }

  formValidation() {
    if (!this.tampiltugas.tampilTugas) {
      // show toast message
      this.showToast("Tugas Terkirim...");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }

}
