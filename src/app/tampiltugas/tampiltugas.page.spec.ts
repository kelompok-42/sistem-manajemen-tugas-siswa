import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TampiltugasPage } from './tampiltugas.page';

describe('TampiltugasPage', () => {
  let component: TampiltugasPage;
  let fixture: ComponentFixture<TampiltugasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TampiltugasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TampiltugasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
