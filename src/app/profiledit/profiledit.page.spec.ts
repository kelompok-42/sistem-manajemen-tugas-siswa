import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProfileditPage } from './profiledit.page';

describe('ProfileditPage', () => {
  let component: ProfileditPage;
  let fixture: ComponentFixture<ProfileditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfileditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
