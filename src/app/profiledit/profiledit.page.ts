import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profiledit',
  templateUrl: './profiledit.page.html',
  styleUrls: ['./profiledit.page.scss'],
})
export class ProfileditPage implements OnInit {
  userId: string;
  name: string;
  email: string;
  nipnisn: number;
  phone: number;

  constructor(
    public auth: AuthService,
    public db: AngularFirestore,
    public loadingCtrl: LoadingController,
    public toastr: ToastController,
    public router: Router
  ) { }

  ngOnInit() {
    this.auth.user$.subscribe(user => {
      this.userId = user.userId;
      this.name = user.userName;
      this.email= user.userEmail;
      this.nipnisn = user.userNipNisn;
      this.phone = user.userPhone;
    })
  }

  async update()
  {
    const loading = await this.loadingCtrl.create({
      message: 'Updating..',
      spinner: 'crescent',
      showBackdrop: true
    });

    loading.present();

    this.db.collection('user').doc(this.userId).set({
      'userName': this.name,
      'userEmail': this.email,
      'userNipNisn': this.nipnisn,
      'userPhone': this.phone,
      'editAt': Date.now()
    },{merge: true})
    .then(() => {
      loading.dismiss();
      this.toast('Update Berhasil!');
      this.router.navigate(['/profil']);
    })
    .catch(error => {
      loading.dismiss();
      this.toast(error.message);
    })
  }

  async toast(message)
  {
    const toast = await this.toastr.create({
      message: message,
      duration: 2000
    });

    toast.present();
  }

}
