import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ForgotsiswaPageRoutingModule } from './forgotsiswa-routing.module';

import { ForgotsiswaPage } from './forgotsiswa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ForgotsiswaPageRoutingModule
  ],
  declarations: [ForgotsiswaPage]
})
export class ForgotsiswaPageModule {}
