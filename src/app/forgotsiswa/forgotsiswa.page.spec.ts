import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ForgotsiswaPage } from './forgotsiswa.page';

describe('ForgotsiswaPage', () => {
  let component: ForgotsiswaPage;
  let fixture: ComponentFixture<ForgotsiswaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgotsiswaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ForgotsiswaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
