import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ForgotsiswaPage } from './forgotsiswa.page';

const routes: Routes = [
  {
    path: '',
    component: ForgotsiswaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ForgotsiswaPageRoutingModule {}
