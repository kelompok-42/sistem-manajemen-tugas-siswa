import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    public loadingCtrl: LoadingController
  ) { }

  async present(msg, position) {
    const alert = await this.loadingCtrl.create({
      message: msg,
      duration: 2000
    });
    alert.present();
  }

}
