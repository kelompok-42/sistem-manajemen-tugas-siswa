import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user$: Observable<User>;
  user: User;
  userPhone: string;

  constructor(
    public db: AngularFirestore,
    public auth: AngularFireAuth,
    public router: Router,
    public LoadingCtrl: LoadingController,
    public toastr: ToastController
  ) {
    this.user$ = this.auth.authState
    .pipe(
      switchMap( user => {

        if(user)
        {
          return this.db.doc<User>(`user/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }

      })
    )
  }
}
