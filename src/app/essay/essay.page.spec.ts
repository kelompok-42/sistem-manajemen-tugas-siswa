import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EssayPage } from './essay.page';

describe('EssayPage', () => {
  let component: EssayPage;
  let fixture: ComponentFixture<EssayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EssayPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EssayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
