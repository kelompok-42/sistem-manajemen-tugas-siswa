// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false, 
   firebase : {
    apiKey: "AIzaSyDxlCJwV5qBiRo4laKI61WJavlQozu2L0E",
  authDomain: "sistem-manajemen-tugas-siswa.firebaseapp.com",
  projectId: "sistem-manajemen-tugas-siswa",
  storageBucket: "sistem-manajemen-tugas-siswa.appspot.com",
  messagingSenderId: "361482246342",
  appId: "1:361482246342:web:b0fd79d39c904c9444dfc7",
  measurementId: "G-6LVEGRM5NM"
  }
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
